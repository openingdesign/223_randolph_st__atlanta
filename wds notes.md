#Basement
- As the idea is to get rid of the terrace at the back not sure where the stair from the back yard would be?
- About Ryan's comment on doors vs windows. I believe we wanted to have a separate access to the basement level when we finish this level afterwards...
- We will also have to look at that plan so we put the proper windows.
- Agree with adding some windows in the garage.
- Agree with having as few doors as possible to access stairs + gives indeed extra spoace for storage. If this is possible.
- Would like to use as little as possible pavement for drive way for drainage and use indeed retaining pond or evacuate across the house on the not usable lot as it slopes down below the driveway level...
#Ground Floor 
##Front Porch
- Does front porch really need to be 8 feet or can this be part of 		variance (was requested in other variance applications)? 
It seems to me that 8 feet deep porch would make the walkway impractical? 
What is the purpose of such a large porch? Cannot see myself using it.
Should we put this as part of the variance like so many other houses? What do you think?
## Office:
- Agree that office could be bigger but a 150 sqft is a nice home office. Right now with current dimensions it is 171.66 sqft. Increasing north wall to 14 to 15' would increase this to 191.625 to 205.3125 sqft.
Any coments on this. 
- A larger office is fine to me but what impact does it have on the pantry and WC? 
##WC
- I agree with using as many common plumbing walls as possible.
Planning to use wall mounted toilets.
I not critical for the variance. 
##Pantry
- A good size pantry is about 50  sqft and this one right now is 76 sqft.
In creasing office will have an impact on this. Right?
Not familiar with a scullery.
##Kitchen and Dining room
- Agree with having a sliding door to the pantry. Not sure if i understand everything in the kitchen. Think it might be better to have also the range on the island and have a vent hood above it. This is not critical for variance.
##Terrace
- Was going to comment on this is because I agree with Ryan that although by increasing garage we had extra space the way we use it for the terrace is not efficient(too narrow).
- It is indeed beter to increase the depth and as such move the living room. I assume this floor will be cantileaved if needed? It just has to be within the total FAR. So Ryan's suggestions is good for me.
#Upper Floor
##Master Bedroom
 - Agree with no acces to second bedroom terrace.
 - Like suggestion about master bedroom changes:
 - Accessing bedroom thorugh walking closet does not make sense
 - Orientation bed suggestion makes sense.
 - What is missing is a shower... Is it possible to have a shower and a tub?
 - Walk-in closet is indeed to big comapred with bathroom.
 - What would be the new size of the walk-in closet?
 - Like the idea of the cantileave for closet & desk and in essence their will be  hallway that give access to master bedroom but will that push us over the FAR?
 	**Conclusion:**
 	- With the changes we would have door to a hallway closet.
 	-  There will be a door that access the master bedroom through a hallway.
 	-  A door that accesses the bathroom and walk-in from the hallway.
 	pocket door to the cantileave closet.
 	- A pocket door to the toilet. 
##Daughter & Guest bedroom
- Access to terrace from both bedrooms
- Shared badroom accesable through hallway
- Adding closet for both bedrooms. Could that be shared on a shared wall?
- Size looks good (between 100 & 200 SQFT)
- Using as much as possible shared interrior plumbing walls
- Would we combine the 2 terraces? By extending the terrace in front on themaster badroom to the same as the smaller terrace? That way the terrace is shared by both bedrooms?
- It would add more terrace that is close to the property line.

 	
 	