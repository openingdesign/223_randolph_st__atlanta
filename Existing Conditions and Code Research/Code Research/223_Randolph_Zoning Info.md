- Address: 223 RANDOLPH ST NE

  - [link](https://gis.atlantaga.gov/propinfo/?location=-9391759.329784526%2C3996686.7565409034) to City of Atlanta Property Information

- R-5 TWO-FAMILY RESIDENTIAL DISTRICT REGULATIONS

- Neighborhood    Old Fourth Ward

- Land Use Description    Low-Density Residential

- ##### [CHAPTER 7. - R-5 TWO-FAMILY RESIDENTIAL DISTRICT REGULATIONS](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH7TMIREDIRE)

  - ##### [Sec. 16-07.008. - Minimum yard requirements.](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH7TMIREDIRE_S16-07.007MILORE)

    - ##### Yard Requirements

      - front yard: 30ft, but can be reduced per the following exception.
        - from [Sec. 16-28.007. - Regular lots](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH28GESURE_S16-28.007RELO): *Average depth front yard: Where more than 50 percent of the frontage within a block between intersecting streets is developed with structures having a lesser setback than required by the applicable district regulations, the setback requirement for proposed structures may be reduced to the average setback so established by the bureau of buildings but shall not be reduced by less than 50 percent of the required setback so contained within the applicable district regulations.
          - The zoning department said the onus is on the architect to document this condition.  We can use an google area to document the setback along the street.
      - side yard: 7ft
        - In speaking with Nathan Brown, since the lot is quite small, he indicated that there might be a good chance to reduce the side yard setback to the north
      - Rear: 7ft

    - ##### Lot area

      - Existing Lot size: .11ac/4792sf (does not meet min. lot requirements of 7500sf, therefore the following applies...)

      - Floor area cannot exceed the lesser of  

        - 3,750 sf **or**
        - a maximum floor area ratio of 0.65 of the net lot area
          - .65 x 4889 = **3178sf** (because lesser, this sf governs)
          - Assuming 2 stories (does not include basement if no habitable rooms)
            - 3115s/2 = 1589 sf/floor
              - In speaking with Nathan Brown, the max. floor area only includes the conditioned space, and does not include covered, outdoor porches/patios/balconies.
              - Per [this](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH28GESURE_S16-28.010DEMEMEREREGESERELI) code, this SF does not include the basement if half the walls of the basement are below grade, and are not habitable rooms.

      - ##### Maximum lot coverage: Cannot exceed 55% of net lot area.

        - 4889sf x .55 = 2689sf

          - Pervious driveway?

            - The zoning department indicated there was no means to use pervious paving, and/or storm water management, such as a rain garden, to lessen the lot coverage.  This is unfortunate, as the max lot coverage will be exceeded by a large percentage with the planned driveway to the south of the site.
	            -  They did indicate however, since the overall lot was a lot smaller than the min. lot of 7500sf that it demonstrates a 'hardship' that can justify the application for a variance to increase this max. lot coverage.

  - ##### [Sec. 16-07.009. - Maximum height](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH7TMIREDIRE_S16-07.009MAHE)

    - Max Height: 35ft

  - ##### [Sec. 16-07.010 - Minimum off-street parking requirements.]((https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH7TMIREDIRE_S16-07.010MIOREPARE))

    - one space per dwelling

  - ##### [Sec. 16-07.012. - Relationship of building to street](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH7TMIREDIRE_S16-07.012REBUST)

    - Front porches
      - Be a minimum of 12 feet wide or one-third the width of the front facade, whichever is greater
      - a minimum of eight feet deep
      - Contain roofs, a minimum of six-inch wide porch roof supports
      - and steps
    - _Garages_ with front-facing _garage_ doors shall be recessed and located a minimum distance of ten linear feet behind the front façade of the principal structure.

- ##### [CHAPTER 28. - GENERAL AND SUPPLEMENTARY REGULATIONS](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH28GESURE)

  - ##### [Sec. 16-28.008. - Required yards and open space, detailed limitations on occupancy](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH28GESURE_S16-28.008REYAOPSPDELIOC)

    - Projections into yard
      - 20inches
    - Porch can encroach 10ft into front yard
    - Walls or fences in required yards; height limits:
      - In the R-1 through R-5 districts, the following retaining walls and fences are permitted:
        - Front yard
          - 4ft
        - side and rear yards
          - no larger than 6ft high

  - ##### [Sec. 16-28.010. - Definitions and methods of measurements relating to the Residential General Sectors 1—5; requirements and limitations.](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH28GESURE_S16-28.010DEMEMEREREGESERELI)

    - Residential floor area: Residential floor area is the sum of areas for residential use on all floors of buildings, measured from the outside faces of the exterior walls, including halls, lobbies, stairways, elevator shafts, enclosed porches and balconies, and below-grade floor areas used for habitation and residential access. Not countable as residential floor area are:

      1. Open terraces, patios, atriums or balconies

      2. Carports, garages, breezeways, tool sheds;

      3. Special-purpose areas for common use of occupants, such as recreation rooms or social halls;

      4. Staff space for therapy or examination in care housing;

      5. Basement space not used for living accommodations; or

      6. Any commercial or other nonresidential space.

      Maximum residential floor area shall not exceed the number of square feet by multiplying gross residential land area by the floor area ratio (FAR) applying in the appropriate R-G number designation.

  - ##### [Sec. 16-28.022. - Height; excluded portions of structures](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH28GESURE_S16-28.022HEEXPOST)

    - *Excluded portion of structures: Except as specifically provided herein, the height limitations of this part shall not apply to any roof structures for housing elevators, stairways, tanks, ventilating fans or similar equipment required to operate and maintain the building, nor to church spires, steeples, belfries, cupolas, domes, monuments, water towers, fire or parapet walls, roof signs, skylights, flagpoles, chimneys, smokestacks, silos, energy generation structures or similar structures, which may be erected above the height limit.*

  - ##### [Sec. 16-28.027. - Main floor level height limitation for single-family, two-family or duplex dwellings]((https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH28GESURE_S16-28.027MAFLLEHELISIMITMIDUDW))

    - *For new construction, the height of the main floor level of any new single-family, two-family or duplex structure shall be measured as the distance between the top of the sub-floor of said level and the grade as established by plans meeting the specifications required for soil erosion and sedimentation control by [section 74-40](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIICOORENOR_CH74EN_ARTIISOERSEPOCO_S74-40PLRE)  (as it may be amended) and shall be no higher than the greatest of the following:* ```The zoning department indicated that (b) applies to this site.```
      - *a: Four feet above the existing undisturbed grade of the lot as grade is determined shown in the manner required in this section; or*
      - *b: Three feet above the average finished grade level at the property line adjacent to any right-of way(s); or*
      - *c: Three feet above the street fronting main floor threshold level of an existing structure that is to be demolished for the construction of a new single-family, two-family or duplex dwelling.*

##### [CHAPTER 29. - DEFINITIONS](https://library.municode.com/ga/atlanta/codes/code_of_ordinances?nodeId=PTIIICOORANDECO_PT16ZO_CH29DE)

- ##### **Floor area ratio:**
	- A number which, when multiplied by the total net lot area of any lot within the R-1 through R-5 district, establishes the total amount of gross floor space which may be built on that lot, excluding basement space but including attic space as each provided by their individual definitions, and excluding garage space and space contained within any accessory structure unless said accessory structure is used as a secondary dwelling unit.

- ##### **Lot coverage:**
	- A percentage factor which, when multiplied by the total area of any lot within the R-1 through R-5 district, establishes the total area of impervious surface which may be built on said lot. Impervious surface shall include the footprint of the main structure, driveways, turnarounds, parking spaces, and all accessory structures including patios, decks, tennis courts, swimming pools and similar structures.

- ##### **Basement:**
	- A story of a building having half or more of its clear height below grade and used for storage, garages for use of occupants of the building or utilities common to the rest of the building.
	- Any habitable area below the main floor level which meets minimum headroom requirements and where no more than 50 percent of the total exterior perimeter walls are exposed more than four feet from the main floor level finished floor to the immediately adjacent finished grade.
    
- ##### **Height:**
	- Building, height of: The vertical distance from grade to the mean level between the lowest and highest points of the roof of the highest story. The height of a building shall be the average building height based on the measurement of all elevations. The height of a building on each elevation shall be measured from the point of the average finished grade level of each elevation to the mean level between the lowest point on the edge of the eave of said elevation and the highest point of the roof over the highest story facing that same said elevation without regard to any intervening roof peak.
	- Grade: The average level of the finished surface of the ground adjacent to the exterior walls of a building.
	- Residential floor area: Residential floor area is the sum of areas for residential use on all floors of buildings, measured from the outside faces of the exterior walls, including halls, lobbies, stairways, elevator shafts, enclosed porches and balconies, and below-grade floor areas used for habitation and residential access. Not countable as residential floor area are:(1)Open terraces, patios, atriums or balconies;(2)Carports, garages, breezeways, tool sheds;(3)Special-purpose areas for common use of occupants, such as recreation rooms or social halls;(4)Staff space for therapy or examination in care housing;(5)Basement space not used for living accommodations; or(6)Any commercial or other nonresidential space.
    
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQ4NTkwMTIxMiwxOTA3MjM1MTYsMTY3NT
Q0MjU5NywxOTE0NDg0NDY2LC02NjgyNDU4MzEsLTcxNzkxNjM4
MSw0OTU5MTk0MzEsLTQ0MzU1ODQxNywxNjk0NTUxMDY5LDE4Mj
czMTA3MiwtNTY5NjY0MTkzLDIxMDA3OTA4NjUsMTI5NTY3NzUz
MywtNTU0MDU0MDg2LC0yMDgwNTMxMzUyLDI1MTc2MjM5MSwxMT
c4NDI1MzM4LDcxNjg5MjMzNywtMTkxMDM4NDA3MCwtODg5OTEx
OTgzXX0=
-->
