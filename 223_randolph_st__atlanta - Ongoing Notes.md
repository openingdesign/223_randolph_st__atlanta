- https://gis.atlantaga.gov/propinfo/?location=-9391759.329784526%2C3996686.7565409034


- 3115sf max
	- 1st floor: 1183sf/1442sf
	- 2nd floor: 1270sf/1550sf
	- Total: 2453sf/2992sf

 - Basement: 1429sf


Inital Questions
---

- Program
  
  - How many bedrooms?
    
    - adjacencies?
      - walkin closets
      - restrooms
      - room next to room
    - Who's all living there?
  
  - How many baths and half baths?
    
    - adjacencies?
  
  - Decks?
    
    - roof deck
      - stair up to roof deck?
    - balconies off rooms?
  
  - Kitchen
    
    - Pantry
    
    - Island
    
    - next to half bath? next to mud room?
  
  - Living room, next to kitchen, open concept?
    
    - Fireplace?
    
    - Surround sound, speakers throughout
  
  - Grand stair case?.. heart of home?
    
    - Open to 2 story spaces?
  
  - Den? Offices?
    
    - near bedrooms, or lving areas?
  
  - Utility?
  
  - Washer/Dryer
    
    - near master? => **basement level?**
  
  - Garage
    
    - Accomdate how many cars in garage?
    
    - Any adjacent uses?
      
      - offices - live/work?
      
      - utility
      
      - workroom?
    
    - Partially exposed basement?
    
    - Generic
      
      - Any big furniture to accomodate?


- Siting
  
  - view to the north? glass on north
  
  - Privacy from road?
  
  - push up to the front or back?
  
  - Partially exposed basement?
  
  - Keep any trees?
  
  - Why driveway on the north?
  
  - front porch?
    
    - required by code


- Landscaping?


- accessory structure now, or in the future?


- Triangle lot
  
  - How do you know there will be nothing built there?
    
    - Anyway to buy triangle lot to the north?


- Balance privacy and window views, etc.?


- Heating?
  
  - Radiant?
  
  - Forced air?
    
    - open to exposed spirals?


- Energy modeling?
  
  - daylighting modeling?


- Explore passive house detailing?


- Casework drawings?


- Door hardware?


- Draw up the utilities?
  
  - Mechanical?
  
  - Plumbing?
  
  - Electrical
    
    - receptacles
    
    - lighting layout
    
    - pick out fixtures?


- Landscape drawings?


- IFC throughout all exterior walls/floor, on all floors?


- Steel studs or wood?


- Wife pinterest boards?


- Budget?


- How important is green construction?


- Drone on site?


- cad file of survey?


- gitlab username
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzMjA2MTQyNzVdfQ==
-->